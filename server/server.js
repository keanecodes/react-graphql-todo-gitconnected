import express from 'express';
import schema from './schema';
import graphHTTP from 'express-graphql';
const port = 3001;
const app = express();

/**
 * Assuming API is public or front-end dev server runs on different port,
 * We need to allow cross-origin requests
 */
app.use('/graphql', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers',
    'Content-Type, Authorization, Content-Length, X-Requested-With');
    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
});

/**
 * Magic lines of code for GraphQL
 */
app.use('/graphql', graphHTTP({
    schema,
    graphiql: true
}))

/**
 * The code that launches the server itself
 */
const server = app.listen(port, () => {
    console.log(
        `\n\nExpress listen at http://localhost:${port} \n`
    );
});