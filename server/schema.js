import Users from './data/users';
import Todos from './data/todos';
import find from 'lodash/find';
import filter from 'lodash/filter';
import sumBy from 'lodash/sumBy';
import {
  GraphQLInt,
  GraphQLBoolean,
  GraphQLString,
  GraphQLList,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema
} from 'graphql';

/**
 * Define data structures for entities we need (Users & Todos)
 */
const UserType = new GraphQLObjectType ({
  name: 'User',
  description: 'Users in company',
  fields: () => ({
    id : {type: new GraphQLNonNull(GraphQLInt)},
    first_name: {type: new GraphQLNonNull(GraphQLString)},
    last_name: {type: new GraphQLNonNull(GraphQLString)},
    email: {type: GraphQLString},
    gender: {type: GraphQLString},
    department: {type: new GraphQLNonNull(GraphQLString)},
    country: {type: new GraphQLNonNull(GraphQLString)},
    /**
     * For these two types related to another entity (todos),
     * we define a resolve function
     */
    todo_count: {
      type: GraphQLInt,
      //Aggregate the number of todos
      resolve: (user) => {
        return sumBy (
          Todos, todo => todo.userId === user.id ? 1 : 0
        );
      }
    },
    todos: {
      type: new GraphQLList(TodoType),
      //Fetch all todos related to user
      resolve: (user, args) => {
        return filter (
          Todos, todo => todo.userId === user.id
        );
      }
    }
  })
});

/**
 * Define similar functions for todos
 * with relation to the user's task assigned to
 */
const TodoType = new GraphQLObjectType({
  name: 'Todo',
  description: 'Task for user',
  fields: () => ({
    id: {type: new GraphQLNonNull(GraphQLInt)},
    title: {type: GraphQLString},
    completed: {type: new GraphQLNonNull(GraphQLBoolean)},
    user: {
      type: UserType,
      resolve: (todo, args) => {
        return find(Users, user => user.id === todo.userId);
      }
    }
  })
});

/**
 * Root Query for our application
 * (fetches data from data source)
 * -> For users, implement filtering parameters
 * -> For Todos, fetch all items related to the userId given
 *
 * Fields: users (UserType), todos (TodoType)
 */
 const TodoQueryRootType = new GraphQLObjectType({
  name: 'TodoAppSchema',
  description: 'Root Todo App Schema',
  fields: () => ({
    users: {
      /**
       * We can filter Users list by four possible
       * parameters defined in args
       */
      args: {
        first_name: {type: GraphQLString},
        last_name: {type: GraphQLString},
        department: {type: GraphQLString},
        country: {type: GraphQLString}
      },
      type: new GraphQLList(UserType),
      description: 'Lists of Users',
      // filter implemented in a resolve function using lodash
      resolve: (parent, args) => {
        if (Object.keys(args).length) {
          return filter(Users, args);
        }
        return Users;
      }
    },
    todos: {
      // filter it by userId and by completed state
      args: {
        userId: {type: GraphQLInt},
        completed: {type: GraphQLBoolean}
      },
      type: new GraphQLList(TodoType),
      description: 'List of Todos',
      resolve: (parent, args) => {
        if (Object.keys(args).length) {
          return filter(Todos, args);
        }
        return Todos;
      }
    }
  })
 });

 /**
  * Create and export our schema object (finally)
  */
const schema = new GraphQLSchema({
  query: TodoQueryRootType
});

export default schema;